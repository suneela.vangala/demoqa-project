/*
@author Suneela Kammari
*/

package com.demoqa.automation.common;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TestBase {

    public static org.openqa.selenium.WebDriver driver;

    public static ExtentTest extTest;
    public static ExtentReports extent;
    
    public final static String BROWSER_NAME = "Chrome";
    public final static String APPPLICATION_URL = "https://demoqa.com";


    public static Date date = new Date();
    public static SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");
    public static String dt = formatter.format(date);
    public static ExtentSparkReporter spark;
    public static String reportDestination = "reports/report_" + dt + ".html";

    
    protected final Logger logger = LogManager.getLogger(getClass());

    @BeforeSuite(alwaysRun = true)
    public void setUp() throws IOException {

    	configureExtentReport();
        
        openBrowser();
        
    }

    @AfterSuite(alwaysRun = true)
    public void tearDown() throws EncryptedDocumentException {

        driver.quit();
        extent.flush();
    }

    @AfterMethod
    public void tearDown(ITestResult result) {

        if (result.getStatus() == ITestResult.FAILURE) {
        	extTest.fail(result.getName() + " test case is failed. " + "<span class='badge badge-danger'> Fail </span>" + result.getThrowable());
        	extTest.fail(new Throwable());
        	extTest.fail(new Exception());

        } else if (result.getStatus() == ITestResult.SKIP) {
        	extTest.skip(result.getName() + " test case is skipped." + "<span class='badge badge-warning'> Skip </span>");

        } else if (result.getStatus() == ITestResult.SUCCESS) {
        	extTest.pass(result.getName() + " test case is Passed." + "<span class='badge badge-success'> Success </span>");
        }
    }

    public void configureExtentReport() {

        spark = new ExtentSparkReporter(reportDestination);
        extent = new ExtentReports();
        extent.attachReporter(spark);
        
       //extent = new ExtentReports();
       extent.setSystemInfo("OS", System.getProperty("os.name"));
       extent.setSystemInfo("Browser Name", BROWSER_NAME);

    }


    public void openURL(String url) {
        driver.get(url);
    }


    public void openBrowser() {

            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
            System.setProperty("webdriver.chrome.logfile", "./logs/chromeLogs.txt");
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            
    }

}