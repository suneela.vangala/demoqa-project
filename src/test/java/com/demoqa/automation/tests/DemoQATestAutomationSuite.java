/*
@author Suneela Kammari
*/

package com.demoqa.automation.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.demoqa.automation.common.TestBase;
import com.demoqa.automation.pages.MainPage;

public class DemoQATestAutomationSuite extends TestBase {



	MainPage mainPage = new MainPage();

	@Test
	public void tc01CheckBox() throws Exception {

		extTest = extent.createTest("Verify CheckBox", "Case 1: ");

		//1. Navigate to site
		openURL(APPPLICATION_URL);
		
		// 2. Select ""Elements"" tile
		clickOnWebElement(driver.findElement(mainPage.elements), "Elements Link");
		
		// 3. Select the ""Check Box"" option from the side menu	
		clickOnWebElement(driver.findElement(mainPage.checkBox), "Check Box");
		
		// 4.Expand the folder tree using the ""+"" button on the top right corner
		clickOnWebElement(driver.findElement(mainPage.expand), "Expand + ");
		
		// 5. Select the checkbox under the file path ""Home"">""Workspace"">""Angular""
		clickOnWebElement(driver.findElement(By.xpath("//span[@class='rct-text']//span[contains(.,'Angular')]")), "Angular Selection");
		//??
		
		//6. Verify the selection 
		if (driver.findElement(By.xpath("//input[@id='tree-node-angular']")).isSelected()) {
			
			extTest.log(Status.PASS, "Angular Checkbox Selected");
				
		} else {
			
			extTest.log(Status.FAIL, "Angular Checkbox UnSelected");
		}
		
		//7. Uncheck the Angula checkbox
		clickOnWebElement(driver.findElement(By.xpath("//span[@class='rct-text']//span[contains(.,'Angular')]")), "Angular Selection");
		
		//8. Verify it is no longer selected""
		if (! driver.findElement(By.xpath("//input[@id='tree-node-angular']")).isSelected()) {
			
			extTest.log(Status.PASS, "Angular Checkbox UnSelected");
			
		} else {
			
			extTest.log(Status.FAIL, "Angular Checkbox Selected");
		}
		
	}

	

	@Test
	public void tc02CheckBox() throws Exception {
		
		extTest = extent.createTest("Verify The Submit Process", "Case 1: ");

		// 1. Navigate to site
		openURL(APPPLICATION_URL);
		

		// 2. Select the "Elements" tile
		clickOnWebElement(driver.findElement(mainPage.elements), "Elements Link");
		
		// 3. Select the "Text Box" option from the side menu
		clickOnWebElement(driver.findElement(mainPage.textBox), "Text Box");
		
		// 4. Enter the relevant test data to the "Full Name", "Current Address", and "Permanent Address" fields
		enterValueToTheTextBox(driver.findElement(mainPage.fullName), "Full Name", "John Doe");
		
		// 5. Enter the invalid email data to the "Email" text field
		enterValueToTheTextBox(driver.findElement(mainPage.emailId), "Email Id", "johnDoe1");
		
		
		enterValueToTheTextBox(driver.findElement(mainPage.currentAddress), "Current Address",
				"78 Test Str, State, 12345");
		
		enterValueToTheTextBox(driver.findElement(mainPage.permanantAddress), "Permanant Address",
				"98 QA Dr, State, 12345");
		
		// 6. Select the "Submit" button
		clickOnWebElement(driver.findElement(mainPage.submitButton), "Submit Button");
		
		// 7. Verify the entry has not been submitted due to invalid email
		if (!(driver.findElement(By.xpath("//*[@id='output']")).isDisplayed())) {
			
			if (driver.findElement(By.xpath("//*[@id='userEmail' and @class='mr-sm-2 field-error form-control']"))
					.isDisplayed()) {
				
				extTest.log(Status.PASS, "Unable to submit as Email Id is invalid");
				
			}
		} else {
			extTest.log(Status.FAIL, "Able to submit with invalid Email I");
		}

	}

	@Test
	public void tc03CheckBox() throws Exception {
		
		extTest = extent.createTest("Verify The Alert Message", "Case 1: ");

		// 1. Navigate to site				
		openURL(APPPLICATION_URL);
		
		// 2. Select the "Alerts, Frame & Windows" tile
		clickOnWebElement(driver.findElement(mainPage.alertsFrameWindows), "Alerts, Frame & Windows");
		
		// 3. Select the "Alerts" option from the side menu
		clickOnWebElement(driver.findElement(mainPage.alertsSection), "Alerts Section");
		
		
		// 4. Click the "On button click, alert will appear after 5 seconds" button
		clickOnWebElement(driver.findElement(mainPage.clickMe), "5 Seconds Alert Click Me");
		if (isAlertPresent())  {
			extTest.log(Status.FAIL, "Alert showed up before 5 seconds");
		}
		
		TimeUnit.SECONDS.sleep(5);
		
		// 5. Verify the alert pop up has appeared
		if (isAlertPresent())  {
			extTest.log(Status.PASS, "Alert showed up after 5 seconds");
		} else {
			extTest.log(Status.FAIL, "Alert is not present");
		}
		
		// 6. Close the alert
		driver.switchTo().alert().accept();

		

	}
	
	@Test
	public void tc04CheckBox() throws Exception {
		
		extTest = extent.createTest("Verify The Submit Process", "Case 1: ");

		openURL(APPPLICATION_URL);
		
		clickOnWebElement(driver.findElement(mainPage.widgets), "Widgets");
		
		clickOnWebElement(driver.findElement(mainPage.datePicker), "Date Picker Section");
		
		clickOnWebElement(driver.findElement(mainPage.selectDate), "Clicked On Select Date");

	}
	


	/**
	 * generic method to click on Web element
	 * @param ele
	 * @param str
	 * @throws Exception
	 */
	public void clickOnWebElement(WebElement ele, String str) throws Exception {
		if (ele.isDisplayed()) {
			ele.click();
			extTest.log(Status.PASS, str + " Clicked");
		} else {
			extTest.log(Status.INFO, str + " not Displayed");
		}
	}

	/**
	 * @param ele
	 * @param str
	 * @param val
	 */
	public void enterValueToTheTextBox(WebElement ele, String str, String val) {
		if (ele.isDisplayed()) {
			ele.sendKeys(val);
			extTest.log(Status.PASS, val + " Entered for " + str);
		} else {
			extTest.log(Status.INFO, str + " not Displayed");
		}
	}



	public boolean isAlertPresent() 
	{ 
	    try 
	    { 
	        driver.switchTo().alert(); 
	        return true; 
	    }   // try 
	    catch (NoAlertPresentException Ex) 
	    { 
	        return false; 
	    }   // catch 
	}   // isAlertPresent()
	
}
