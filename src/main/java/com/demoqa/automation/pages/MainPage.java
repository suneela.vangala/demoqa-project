package com.demoqa.automation.pages;

import org.openqa.selenium.By;


public class MainPage {

   
    public By elements = By.xpath("//h5[contains(.,'Elements')]");
    
    public By checkBox = By.xpath("//span[contains(.,'Check Box')]");
    
    public By textBox = By.xpath("//span[contains(.,'Text Box')]");
    
    public By expand = By.xpath("//button[@title='Expand all']");
    
    public By fullName = By.xpath("//*[@id='userName']");
    
    public By emailId = By.xpath("//*[@id ='userEmail']");
    
    public By currentAddress = By.xpath("//*[@id='currentAddress']");
    
    public By permanantAddress = By.xpath("//*[@id ='permanentAddress']");
    
    public By submitButton = By.xpath("//*[@id ='submit']");
    
    public By alertsFrameWindows = By.xpath("//h5[contains(.,'Alerts, Frame & Windows')]");
    
    public By alertsSection = By.xpath("//li[contains(.,'Alerts')]");
    
    public By clickMe = By.xpath("//*[@id ='timerAlertButton']");
    
    public By widgets = By.xpath("//h5[contains(.,'Widgets')]");
    
    public By datePicker = By.xpath("//span[contains(.,'Date Picker')]");
    
    public By selectDate = By.xpath("//*[@id='datePickerMonthYearInput']");
    
  

 

     
}
